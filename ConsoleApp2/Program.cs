using System;
using System.Collections.Generic;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Dialog winDialog = new WindowsDialog();
            Dialog webDialog = new WebDialog();

           var da =  winDialog.NewDialog();
           var dd = webDialog.NewDialog();

            Console.WriteLine(da);
            Console.WriteLine(dd);
            Console.Read();
        }
    }



    abstract class Dialog
    {
        public abstract IButton FactoryMethod();


        public string NewDialog()
        {
            var dialog = FactoryMethod();

            var sq = dialog.OnClick();
            return sq;

        }

    }

    public interface IButton
    {
        string OnClick();
    }

    class WindowsButton : IButton
    {
        string IButton.OnClick()
        {
            return "������ Windows ������";
        }
    }

    class HtmlButton : IButton
    {
        string IButton.OnClick()
        {
            return "������ �� Web ������";
        }
    }


    class WindowsDialog : Dialog
    {
        public override IButton FactoryMethod()
        {
            return new WindowsButton();
        }
    }

    class WebDialog : Dialog
    {
        public override IButton FactoryMethod()
        {
            return  new HtmlButton();
        }
    }

}
